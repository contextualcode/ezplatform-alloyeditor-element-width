import React, {Component} from 'react';
import PropTypes from 'prop-types';

const DEFAULT_WIDTH_VALUE = 'auto';
// https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#Lengths
//const WIDTH_UNITS = ['%', 'px', 'cm', 'mm', 'Q', 'in', 'pt', 'pc', 'em', 'ex', 'ch', 'lh', 'vw', 'vh', 'rem', 'vmin', 'vmax'];
const WIDTH_UNITS = ['px', '%'];

class BtnElementWidth extends Component {
    static get key() {
        return 'elementwidth';
    }

    constructor(props) {
        super(props);

        this.state = {
            widthValue: DEFAULT_WIDTH_VALUE,
            widthUnit: WIDTH_UNITS[0],
        };
    }

    updateWidthValue(event) {
        const widthValue = parseInt(event.nativeEvent.target.value);
        const {widthUnit} = this.getCurrentWidth();

        this.updateCurrentElementWidth(widthValue, widthUnit);
        this.fireEditorInteraction(event);
        this.setState({widthValue});
    }

    updateWidthUnit(event) {
        const widthUnit = event.nativeEvent.target.value;
        const {widthValue} = this.getCurrentWidth();

        this.updateCurrentElementWidth(widthValue, widthUnit);
        this.fireEditorInteraction(event);
        this.setState({widthUnit});
    }

    fireEditorInteraction(event) {
        const editor = this.props.editor.get('nativeEditor');
        editor.fire('change', {editor, data: event, name: 'update-element-width'});
    }

    updateCurrentElementWidth(value, unit) {
        const element = this.getCurrentElement();
        if (!value) {
            element.removeStyle('width');
        } else {
            element.setStyle('width', value + unit);
        }

        element.data('width-unit', unit);
    }

    getCurrentWidth() {
        const element = this.getCurrentElement();
        const width = element.getStyle('width');
        const matches = width.match(/([-+]?[\d.]*)(.*)/);
        const lastSetWidthUnit = element.data('width-unit');
        const defaultUnit = lastSetWidthUnit ? lastSetWidthUnit : WIDTH_UNITS[0];

        return {
            widthValue: matches[1] ? matches[1] : '',
            widthUnit: matches[2] && WIDTH_UNITS.includes(matches[2]) ? matches[2] : defaultUnit,
        };
    }

    getCurrentElement() {
        return this.props.editor.get('nativeEditor').elementPath().lastElement;
    }

    render() {
        const {widthValue, widthUnit} = this.getCurrentWidth();

        return (
            <div class="ez-btn-ae--element-width">
                <input
                    type="text"
                    value={widthValue}
                    placeholder={DEFAULT_WIDTH_VALUE}
                    onChange={this.updateWidthValue.bind(this)}
                />
                <select onChange={this.updateWidthUnit.bind(this)}>
                    {WIDTH_UNITS.map((unit) => <option value={unit} selected={widthUnit == unit}>{unit}</option>)}
                </select>
            </div>
        );
    }
}
BtnElementWidth.propTypes = {
    editor: PropTypes.object.isRequired,
    tabIndex: PropTypes.number.isRequired,
};

AlloyEditor.Buttons[BtnElementWidth.key] = AlloyEditor.BtnElementWidth = BtnElementWidth;