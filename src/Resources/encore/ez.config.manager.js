const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/element-width.js'),
        ]
    });
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-css',
        newItems: [
            path.resolve(__dirname, '../public/css/alloyeditor/buttons/element-width.css'),
        ]
    });
};
